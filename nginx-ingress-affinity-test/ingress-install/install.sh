#!/bin/bash
set -e

#helm init --client-only
#helm repo update
helm dependency update

helm install \
  --name zuzu-ing \
  .
