#!/bin/bash
set -e
ip=$(kubectl get svc zuzu-ing-nginx-ingress-controller --no-headers|awk '{ print $4}')

#echo "'$ip'"
#exit 0

cookie=$(curl -s -I http://$ip/ -H 'Host: zuzu.com' | grep 'Set-Cookie'|awk '{ print $2}')

for i in {1..50}
do
  res=$(curl -s http://$ip/ -H 'Host: zuzu.com' -H "Cookie: $cookie")
  echo "$res"
done

