#!/bin/bash
set -e

#input parameter (will be passed by ENV variables)
PROJECT=my-project
SA_KEY=keys/testadmin@my-project.iam.gserviceaccount.com.json

# Detect this service email
SA_EMAIL=$(jq -r .client_email "$SA_KEY")

gcloud config set project $PROJECT
gcloud auth activate-service-account --key-file $SA_KEY


function rotate_keys() {
  local email=$1
  local old_keys=$2

  # Create new key for this service account
  gcloud iam service-accounts keys create "--iam-account=$email" "keys/$email.json"

  if [ "$email" == "$SA_EMAIL" ]; then
    echo "This is my key. Need to relogin."
    gcloud auth activate-service-account --key-file "keys/$email.json"
  fi

  # Removing all old keys
  echo "$old_keys" | \
  while read ok; do
    gcloud iam service-accounts keys delete "$ok" --iam-account="$email" --quiet
  done
}


function handle_sa() {
  local email=$1
  echo "Processing service account '$email' ..."

  keys=$(gcloud iam service-accounts keys list --iam-account "$email" --format="table[no-heading](KEY_ID)" --managed-by=user)
  if [ -z "$keys" ]; then
    echo "> no keys for rotation."
  else
    rotate_keys "$email" "$keys"
  fi
}


function handle_all_sa() {
  local accounts=$(gcloud iam service-accounts list --format="csv[no-heading](EMAIL)")

  echo "$accounts" | \
  while read sa; do
    handle_sa "$sa"
  done
}

handle_all_sa
